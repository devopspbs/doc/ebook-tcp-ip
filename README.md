# Introdução a Redes TCP/IP e MikroTik RouterOS

Esse e-book é uma iniciativa do grupo **[DevOpsPBS](https://gitlab.com/devopspbs/)**.

O objetivo desse projeto é produzir um e-book para iniciantes em Redes TCP/IP e em MikroTik RouterOS.

* **Autores:**
  - [Tiago Rocha](https://tiagorocha.eti.br/)

## Sumário

1. [Introdução](manuscript/intro.md)
1. [A Internet](manuscript/internet.md)
1. [MikroTik RouterOS](manuscript/mt-ros.md)
1. [RouterOS no GNS3](manuscript/gns3-mt-ros.md)
1. [Acesso ao RouterOS](manuscript/mt-ros-access.md)
1. [Lab RouterOS CLI](manuscript/lab-mt-ros-cli.md)
1. [Endereços, Máscaras e DHCP](manuscript/ip-mask-dhcp.md)
1. [Modelos OSI e TCP/IP](manuscript/osi-tcp-ip.md)
1. [Lab Configuração de Cliente DHCP](manuscript/lab-dhcp-client.md)
1. [Lab Configurações Básicas](manuscript/lab-baseline.md)
1. [Lab RouterOS Backup/Restore](manuscript/lab-backup-restore.md)
1. [Serviço de DNS](manuscript/dns.md)
1. [Lab Configuração de DNS](manuscript/lab-dns.md)
1. [Lab Servidor DHCP](manuscript/lab-dhcp-server.md)
1. [Roteamento, Default Gateway e NAT](manuscript/route-gw-nat.md)
1. [Lab Default Gateway e NAT](manuscript/lab-gw-nat.md)
1. [Lab Roteamento Estático](manuscript/lab-routing.md)
1. [CIDR e Sub-redes](manuscript/cidr-subnet-masking.md)
1. [Lab CIDR e Sub-redes](manuscript/lab-cidr-subnet.md)
1. [Referências](manuscript/references.md)
1. [Glossário](manuscript/glossary.md)
1. [Apêndice A - Instalação do GNS3 no Windows](manuscript/apx-gns3-windows-install.md)
1. [Apêndice B - Instalação do GNS3 no GNU/Linux](manuscript/apx-gns3-gnu-linux-install.md)
1. [Apêndice C - Configuração de Rede no Windows](manuscript/apx-network-windows.md)
1. [Apêndice D - Configuração de Rede no GNU/Linux](manuscript/apx-network-gnu-linux.md)

## Licença

GFDL 1.3 or any later version
