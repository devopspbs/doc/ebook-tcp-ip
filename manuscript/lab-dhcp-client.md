# Lab Configuração de Cliente DHCP

## Se Preparando

1. Crie um novo projeto no GNS3;
1. Adicione um appliance routerOS;
1. Adicione uma "nuvem NAT";
1. Link o appliance a nuvem .

![](images/lab-dhcp-client.png)

## Como Fazer

### WinBox

No GNS3 um appliance de nuvem "linkado" ao appliance MikroTik CHR equivale ao nosso computador conectado ao dispositivo no mundo físico. Por isso conseguimos usar o WinBox para acessar o RouterOS tanto pelo endereço IP quanto pelo endereço MAC.

Sabendo que a nossa máquina virtual MikroTik CHR cria uma configuração de DHCP Client sem firewall na porta 1, e que as demais portas não tem DHCP Server configurado, vamos reservar sempre a porta 1 dos nossos dispositivos MikroTik virtuais para conectar ao nosso computador por meio da **nuvem NAT**.

![](images/gns3-mt-ros-start.png)

Pode levar alguns minutos para iniciar a VM e ela obter endereço IP, mas se tudo estiver correto ela sempre aparece na aba **Neighbors** do Winbox. Então podemos acessar nosso RouterOS clicando sobre o endereço IP ou MAC, em seguida digitando o usuário **admin** com o campo password em branco.

![](images/winbox-mac.png)

Como nesse laboratório nós queremos justamente fazer a configuração de um cliente DHCP no RouterOS vamos nos conectar via WinBox usando o endereço MAC.

Vamos começar o lab removendo o cliente DHCP criado automaticamente pelo CHR. Para isso vamos em **IP > DHCP Client**, na janela do cliente DHCP vamos clicar no botão **Remove** (símbolo de subtração vermelho).

![](images/winbox-dhcp-client-remove.png)

Em seguida vamos em **IP > DHCP Client**, na janela do cliente DHCP vamos clicar no botão **Add** (símbolo de adição azul). Por fim vamos nos certificar de selecionar a **Interface** corretamente conectada ao nosso elemento nuvem, que a opção **Use Peer DNS** está marcada, e em seguida clicamos no botão **OK** para aplicar a configuração e fechar a janela.

![](images/winbox-dhcp-client.png)

Pronto! Nesse momento nosso RouterOS deve obter automaticamente uma configuração de rede e conseguir navegar na internet.

Para conferir se nosso cliente DHCP obteve o endereço IP como o esperado podemos verificar o **Log** na interface do WinBox. E para testar nossa conectividade podemos clicar em **New Terminal** e executar o comando ping (digitar a linha comando seguida de um toque na tecla Enter).

```
ping 1.1.1.1
```


### CLI

Embora geralmente os usuários da MikroTik prefiram usar o WinBox para executar as tarefas de gerencia e configuração tudo pode ser feito diretamente na linha de comando, por exemplo, fazendo um acesso remoto via SSH, ou no nosso caso usando a opção **Console** no menu de contexto do appliance no GNS3.

![](images/gns3-mt-ros-console.png)

Perceba que os comandos são idênticos a navegação e cliques realizados na interface do WinBox:

**Remover cliente DHCP:**

```
ip dhcp-client print
ip dhcp-client remove numbers=0
```

**Adicionar cliente DHCP:**

```
ip dhcp-client print
ip dhcp-client add interface=ether1 disabled=no
```

**Verificar os Logs:**

```
log print
```

**Verificar detalhes da conf do cliente DHCP:**

```
ip dhcp-client print detail
```

**"Pingar" endereço IP:**

```
ping 1.1.1.1
```
