# Lab Configuração de Cliente DNS

## Se Preparando

Caso não tenha o lab anterior para reaproveita, refaça-o com os seguintes passos:

1. Incie um novo projeto no GNS3;
1. Adicione um appliance routerOS;
1. Adicione uma "nuvem NAT";
1. Link o appliance a nuvem.

## Como Fazer

### WinBox

Com uma configuração de cliente DHCP funcionando abra um **New Terminal** e rode o comando `ping registro.br`. Perceba que o domínio sera convertido para o endereço IP e ele responderá ao nosso ping.

Agora vamos em **IP > DHCP Client**, na janela do cliente DHCP, na **Aba DHCP** desmarque a opção **Use Peer DNS**, e em seguida clicamos no botão **OK**.

Perceba que agora ao repetir o teste do `ping registro.br` vamos receber uma mensagem de erro pois não contamos mais com um servidor DNS configurado no RouterOS para fazer a "tradução" do nome em um endereço IP.

```
ping registro.br
invalid value for argument address:
    invalid value of mac-address, mac address required
    invalid value for argument ipv6-address
    while resolving ip-address: could not get answer from dns server
```

Vamos deixar nosso RouterOS funcionando normalmente, para isso vamos configurar manualmente nossos endereços de DNS.

Primeiro clicamos em **IP > DNS**, na janela **DNS Settings** clicamos na **seta para baixo** da opção **Servers**, digitamos um dos endereços IP da tabela abaixo de exemplos de servidores DNS. E por fim clicamos no botão **OK** para aplicar a configuração e fechar a janela de configuração.

| Endereços IPs                   | Descrição                                                                                         |
|:------------------------------- |:------------------------------------------------------------------------------------------------- |
|        1.1.1.1 e 1.0.0.1        | Servidores DNS públicos da CloudFlare, geralmente apresentam o melhor desempenho.                 |
|   189.38.95.95 e 189.38.95.96   | Servidores públicos de DNS brasileiros, geralmente apresentam um ótimo desempenho.                |
| 208.67.222.222 e 208.67.220.220 | Servidores DNS públicos Cisco OpenDNS.                                                            |
|        8.8.8.8 e 8.8.4.4        | Servidores DNS Google, não recomendados devidos a problemas frequentes e questões de privacidade. |

> Dica: Configure pelo menos dois endereços de DNS para o caso de falha do primeiro servidor.

Por fim repita o teste do ping e confira se a conversão de nomes em endereços IPs está funcionando corretamente.

### CLI

**Desativar Use Peer DNS no cliente DHCP:**

```
ip dhcp-client set numbers=0 use-peer-dns=no
```

> Atenção: Perceba que o valor **numbers** refere-se ao cliente DHCP que desejamos mudar a configuração, e que a contagem dos itens (os clientes nesse caso) começam a partir do número zero.

**Configurar servidores DNS:**

```
ip dns set servers=1.1.1.1,189.38.95.95
```
