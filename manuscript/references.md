# Referências

**Wiki MikroTik:** Documentação Oficial da MikroTik  
**Ler On-line:** https://wiki.mikrotik.com/wiki/Main_Page

**Livro:** Redes, Guia Prático 2ª Ed. (Atualização)  
**Autor:** Carlos E. Morimoto  
**Ler On-line:** https://www.hardware.com.br/livros/redes/

**Livro:** Redes de Computadores  
**Autor:** Andrew S. Tanenbaum  
**Comprar:** https://www.estantevirtual.com.br/livros/andrew-s-tanenbaum/redes-de-computadores/2516815423?q=Redes+de+Computadores

**Livro:** Redes de Computadores e a Internet - uma Nova Abordagem  
**Autor:** James F. Kurose / Keith W. Ross  
**Comprar:** https://www.estantevirtual.com.br/livros/james-f-kurose-keith-w-ross/redes-de-computadores-e-a-internet-uma-nova-abordagem/2190281067

**Livro:** Entendendo redes TCP/IP com MikroTik: Teoria e prática  
**Autor:** Romuel Dias de Oliveira  
**Comprar:** https://juliobattisti.com.br/loja/detalheproduto.asp?CodigoLivro=LIV0001550
