# MikroTik RouterOS

## MikroTik

MikroTik é uma empresa da Letônia especializada na fabricação de produtos de rede de computadores para Provedores de Acesso à Internet, Empresas e Residências.

## RouterOS

RouterOS é o Sistema Operacional da MikroTik para roteadores, as famosas Routers Boards.

## RouterBoard

O termo **RouterBoard**, ou simplesmente **RB**, se refere a uma série de linhas de produtos de hardware próprio da MikroTik. As RBs variam de produtos para residências e pequenos escritórios até produtos para empresas e provedores de internet de médio e grande porte, vão de roteadores a dispositivos de rádio, de Caixas completas com cases bem acabados à placas avulsas.

## Cloud Core Routers

Atualmente a MikroTik conta com uma linha de produtos chamadas de **Cloud Core**, nela que temos os **Cloud Core Routers** ou **CCR**, que são basicamente uma evolução das RBs, na verdade apenas com um nome novo, digamos mais "moderno", mais "vendável". Provavelmente a ideia seja justamente aproveitar-se um pouco da popularidade e grande visibilidade do termo "Cloud Computing".
