# Modelos OSI e TCP/IP

## Modelo OSI

Modelo de camadas OSI é o modelo de rede de computadores preferido entre 11 de cada 10 acadêmicos e autores de livros de rede de computadores.

| Camada           | Função                                                                               |
| ---------------- | ------------------------------------------------------------------------------------ |
| 7 - Aplicação    | Funções especialistas (transferência de arquivos, envio de e-mail, terminal virtual) |
| 6 - Apresentação | Formatação dos dados, conversão de códigos e caracteres                              |
| 5 - Sessão       | Negociação e conexão com outros nós, analogia                                        |
| 4 - Transporte   | Oferece métodos para a entrega de dados ponto-a-ponto                                |
| 3 - Rede         | Roteamento de pacotes em uma ou várias redes                                         |
| 2 - Enlace       | Detecção de erros                                                                    |
| 1 - Física       | Transmissão e recepção dos bits brutos através do meio físico de transmissão         |

## Modelo TCP/IP

|  Modelo TCP/IP                  | Modelo OSI                                  |
| ------------------------------- | ------------------------------------------- |
| 4 - Aplicação                   | 5 - Sessão, 6 - Apresentação, 7 - Aplicação |
| 3 - Transporte                  | 4 - Transporte                              |
| 2 - Internet                    | 3 - Rede                                    |
| 1 - Enlace (Interface com Rede) | 1 - Física, 2 - Enlace                      |

## Protocolos por Camadas

|  Modelo TCP/IP                  | Protocolo                                                                      |
| ------------------------------- | ------------------------------------------------------------------------------ |
| 4 - Aplicação                   | BGP, DHCP, DNS, FTP, HTTP, IMAP, LDAP, NTP, POP, SIP, SMTP, SNMP, SSH, TLS/SSL |
| 3 - Transporte                  | TCP, UDP, RTP, DCCP, SCTP, RSVP                                                |
| 2 - Internet                    | OSPF, IP (IPv4 e IPv6), ICMP, ICMPv6, ECN, IGMP, IPsec                         |
| 1 - Enlace (Interface com Rede) | NDP, ARP, L2TP, PPP, MAC, Ethernet, DSL, RDIS, FDDI                            |
