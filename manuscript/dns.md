# DNS

O DNS (domain name system) é o protocolo que permite que nós consigamos acessar sites na internet sem precisar memorizar os endereços IPs dos servidores. Graças a ele podemos usar nomes amigáveis como "meetup.com", seja qual for o endereço IP, ou se preocupar se IP mudou, ou coisa parecida.
 
Perceba a importância que DNS têm, caso tenhamos problemas com o serviço de DNS isso pode ser percebido pelo usuário como ausência de conexão com à internet, isso porque não teremos a conversão dos endereços web em endereços IPs o que impossibilita a abertura de sites e correto funcionamento de serviços, apps e etc.
