# Endereços, Máscaras e DHCP

## Endereços IP

Para que os dispositivos em uma rede possam se comunicar eles precisam de endereços, o responsável por esse endereçamento é o IP (Internet Protocol). Cada endereço IP deve ser único na sua respectiva rede.

Atualmente o IP conta com duas versões, IP versão 4 ou **IPv4**, e IP versão 6 ou **IPv6**. Nesse texto vamos tratar apenas do IPv4.

Os endereços IP versão 4 são divididos em 4 blocos de 8 bits (32 bits no total), e são representados através de números de 0 a 255, ou seja as 256 possibilidades permitidas por 8 bits. Cada grupo de 8 bits é chamado de **octeto**. Exemplos de endereços IPv4: 

192.168.88.17  
1.1.1.1  
8.8.8.8  

## Máscara

Os endereços IPs são divididos em classes e nós representamos isso por meio da **máscara de rede**. Por exemplo:

Endereço IP: 192.168.88.2  
Máscara: 255.255.255.0  

Máscara por Classe:

| **Classe**   | **Máscara de rede** | **Prefixo** |
|------------- | ------------------- | ----------- |
| Classe A     | 255.0.0.0           | /8          |
| Classe B     | 255.255.0.0         | /16         |
| Classe C     | 255.255.255.0       | /24         |

## Classes e IPs Privados

Os endereços IPs foram divididos em classes para agilizar o processo de roteamento.

| **Classe** | **1º Octeto** | **2º Octeto** | **3º Octeto** | **4º Octeto** |
|:----------:|:-------------:|:-------------:|:-------------:|:-------------:|
| Classe A   |     1 a 126   |  **0 a 255**  |  **0 a 255**  |  **0 a 255**  |
| Classe B   |   128 a 191   |    0 a 255    |  **0 a 255**  |  **0 a 255**  |
| Classe C   |   192 a 223   |    0 a 255    |    0 a 255    |  **0 a 255**  |

Baseado nas classes temos as seguintes porções dos endereços para rede e dispositivo (ou host):

|          | 255  | 255      | 255      | 255      |
|:--------:|:----:|:--------:|:--------:|:--------:|
| Classe A | Rede | **Host** | **Host** | **Host** |
| Classe B | Rede |   Rede   | **Host** | **Host** |
| Classe C | Rede |   Rede   |   Rede   | **Host** |

Alguns intervalos de endereços IP são reservados para serem usados nas nossas redes locais. Esses endereços não são roteáveis na internet, quando um pacote com origem ou destino para alguma dessas redes chega a um roteador do provedor/operadora é descartado.

Tabela Intervalo de Endereços Privados:

| **Intervalo de Endereços**    | **Número de Endereços** | **Descrição**      |
|:-----------------------------:| -----------------------:|:------------------ |
| 10.0.0.0 – 10.255.255.255     |              16 777 216 | Uma rede Classe A  |
| 172.16.0.0 – 172.31.255.255   |               1 048 576 | 16 redes Classe B  |
| 192.168.0.0 – 192.168.255.255 |                  65 536 | 256 redes Classe C |

## DHCP

O DHCP (Dynamic Host Configuration Protocol) é o protocolo que permite que os dispositivos recebam suas configurações de rede automaticamente a partir de um servidor central, o que torna desnecessário configurar os dispositivos de um por um.

**DHCP Server:** Aplicação servidora, ela é a responsável por fornecer a configuração de rede para os hosts.

**DHCP Client:** Aplicação cliente, é quem se comunica com o servidor obtendo a configuração, e também de tempos em tempos renova a configuração de rede.
