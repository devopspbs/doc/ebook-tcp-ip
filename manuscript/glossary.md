# Glossário

Estão nesse glossário apenas termos que não são facilmente encontrados em um dicionário ou em uma rápida e preguiçosa pesquisa na internet, ou então termos que têm um significado diferente num contexto de rede de computadores e telecomunicações.

**Appliance:** Um ativo de rede de computadores, por exemplos, switch, roteador, firewall, etc.

**Ativo de Rede:** Dispositivo geralmente gerenciável que tem um papel importante em uma rede, com por exemplo, switch, roteador, firewall, servidor, etc.

**Caixa:** Jargão técnico geralmente usado por profissionais de ISP para se referir a uma solução (hardware e software) de rede de computadores, por exemplo, os roteadores são chamados de "caixas".

**Linux:** Jargão técnico geralmente usado pra se referir a um sistema operacional baseado no kernel Linux.
