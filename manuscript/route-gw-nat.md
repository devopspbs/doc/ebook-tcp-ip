# Roteamento, Default Gateway e NAT

## Roteamento

Em rede computadores nós chamamos de roteamento o processo de reencaminhamento de pacotes baseados em endereços IP e máscaras de rede.

![](https://community.fs.com/blog/wp-content/uploads/2017/10/How-routers-route-packets-from-the-source-to-the-destination.jpg)

## Default Gateway

O default gateway, ou a **rota padrão**, é o portão de entrada e de saída da rede. Ele é o roteador que está interligado a outras redes, e assim, rede após rede estamos conectado a até à internet.

## NAT

Network Address Translation (NAT) é uma técnica avançada de roteamento que permite que vários dispositivos em uma rede acessem a Internet por meio de um único com endereço IP público. Com a escassez dos endereços IPv4 essa técnica foi muito importante para a manutenção do crescimento constantes de dispositivo conectados à internet.
