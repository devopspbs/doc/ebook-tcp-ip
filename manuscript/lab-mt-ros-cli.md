# Lab RouterOS CLI

## Se Preparando

1. Crie um novo projeto no GNS3;
1. Adicione um appliance routerOS;
1. Inicie a appliance;
1. Abra o console.

### Iniciar Appliance

![](images/gns3-mt-ros-start.png)

### Acessar Console

![](images/gns3-mt-ros-console.png)

## Como Fazer

No console vamos logar com o usuário **admin** e a **senha em branco**.

![](images/mt-ros-console-login.png)

Como esse é o primeiro login no CHR somos perguntados se gostaríamos de ler a licença, podemos teclar **N** para responder que não, ou teclar **Enter** para sim.

![](images/mt-ros-console-license.png)

O **[admin@MikroTik] >** indica que o shell do nosso MikroTik RouterOS está pronto para receber comandos. Antes do **@** nós temos o nome do usuário logado, neste caso **admin**, e depois o nome do nosso dispositivo que por padrão é **MikroTik**.

No shell do RouterOS os comandos são digitados após o prompt, **>**. Nós indicamos que o comando deve ser executado teclando **Enter**.

![](images/mt-ros-console-command.png)

### Menu Hierárquico

Devido a grande quantidade de comandos disponíveis eles são divididos em grupos e organizados de forma hierárquica.

![](images/mt-ros-console-hierarchy.png)

O prompt de comando reflete o nível hierárquico do menu, por exemplo, `[admin@MikroTik] ip dhcp-client>`. A barra "/" pode ser usada tanto para voltar novamente ao nível mais alto da hierarquia, quanto para executar um comando "top level" a partir de qualquer nível do menu. Os ".." movem um nível hierárquico para cima, perceba que os dois pontos também podem ser usados para executar um comando de um nível acima.

### Colorido

O shell do RouterOS têm uma variedade interessantes de cores para facilitar nosso trabalho. Abaixo temos uma tabela com algumas cores que é importante ter em mente seus significados durante o uso da linha de comando dos dispositivos da MikroTik.

| **Cor**     | **Descrição**                                                     |
| ----------- | ----------------------------------------------------------------- |
| Azul        | Indica um nível hierárquico do menu.                              |
| Cor de Rosa | Cor de um comando ou recurso do shell (linguagem de programação). |
| Verde       | Indica opções e parâmetros dos comandos.                          |
| Vermelho    | Indica que o/a comando/opção não existe, ou erro de digitação.    |

### Completion

De forma similar ao shell de sistemas Unix-like o RouterOS tem um recurso muito legal que é a tecla de completion **Tab**. Ele funciona de forma bem simples, basicamente o usuário digita as primeiras letras do comando, opção, parâmetro, etc, seguido de um Tab e o shell completa as letras seguintes. Por isso é sempre bom usar o Tab para agilizar a digitação das linhas de comandos e prevenir os erros de digitação.

Agora vamos usar um pouco do completion. Primeiro digite as teclas como visto a seguir:

`in[Tab]pr[Tab][Enter]`

`ip dh[Tab]c[Tab]p[Tab][Enter]`

Perceba que quando existe mais de uma opção para as primeiras letras digitadas o shell não completa, chegando a exibir em vermelho as letras enquanto não temos um comando ou opção digitado corretamente. E que ao teclar Tab duas vezes seguidas rapidamente ele exibe todas as opções possíveis de competion naquele contexto.

`i[Tab][Tab]`

`in[Tab]p[Tab][Tab]`

`in[Tab]pp[Tab][Tab]`

`in[Tab]ppp[Tab][Tab]`

`in[Tab]pppo[Tab][Tab][Tab]`

### Pedindo Ajuda

A primeira forma de conseguir ajuda no terminal nos acabamos de ver. É justamente teclar Tab duas vezes seguidas rapidamente, com isso nós conseguimos ver as possibilidades de comandos, opções, parâmetros e níveis de menu.

A segunda forma é usar digitar o símbolo de interrogação, **?**. Embora também exiba todas as possibilidades para aquele contexto o uso do **?** tem a vantagem de exibir uma pequena descrição que nos ajuda a saber o que os comandos, opções, parâmetros e níveis de menu fazem.

![](images/mt-ros-console-help.png)

### Atalhos

| **Teclas/Combinação** | **Decrição**                                      |
|---------------------- | ------------------------------------------------- |
| F1 ou ?               | Exibe ajuda do contexto.                          |
| Ctrl+C                | Teclas de interrupção.                            |
| Ctrl+D                | Logout (mas só quando a linha estiver vazia).     |
| Delete                | Remove caractere após o cursor.                   |
| Ctrl+H ou Backspace   | Remove caractere antes do cursor.                 |
| Ctrl+B ou Esquerda    | Move o cursor um caractere para trás.             |
| Ctrl+F ou Direita     | Move o cursor um caractere para frente.           |
| Ctrl+P ou Cima        | Vai para linha de comando anterior do histórico.  |
| Ctrl+N ou Baixo       | Vai para a próxima linha de comando do histórico. |
| Ctrl+A ou Home        | Move o cursor para o começo da linha de comando.  |
| Ctrl+E ou End         | Move o cursor para o final da linha de comando.   |
| Ctrl+L ou F5          | "Limpa" a tela do console.                        |

## Tem Mais

Não deixe de ler a documentação oficial da MikroTik para mais detalhes sobre o uso da linha de comando:

https://wiki.mikrotik.com/wiki/Manual:Console
