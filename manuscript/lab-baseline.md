# Lab Configurações Básicas

## Se Preparando

1. Crie um novo projeto no GNS3;
1. Adicione um appliance routerOS;
1. Adicione uma "nuvem NAT";
1. Link o appliance a nuvem .

![](images/lab-dhcp-client.png)

## Como Fazer

### Winbox

#### Identificação

![](images/mt-ros-winbox-identity.png)

1. Clique em **System**.
2. Depois clique em **Identity**.
3. Na janela Identity escreva o nome do dispositivo.
4. Clique no botão **OK** aplicar e fechar a janela.

#### Usuário Administrador

![](images/mt-ros-winbox-users.png)

1. Clique em **System**.
2. Depois clique em **Users**.
3. Na janela **User list** clique na aba **Users**.
4. De um duplo clique sobre o nome do usuário.
5. Configure o nome do usuário desejado.
6. Clique no botão **Apply** para aplicar a configuração de nome do usuário.
7. Para configurar uma senha clique no botão **Password**.
8. Digita a senha e confirme digitando novamente.
9. Clique no botão **OK** para aplicar e fechar.

#### Romon

![](images/mt-ros-winbox-romon.png)

> **Atenção:** Do ponto de vista da segurança é melhor manter o Romon desativado.

#### Serviços

É uma boa prática desativar qualquer serviço que você não vá usar.

![](images/mt-ros-winbox-services.png)

### CLI

Agora para praticar o uso da linha de comando do RouterOS vamos refazer o laboratório no console.

#### Identificação

```
system identity set name=R1
```

#### Usuário Administrador

**Mudar nome do admin e adicionar uma senha:**

```
user set admin name=maria password=passwd123
```

#### Romon

```
tool romon set enabled=yes secrets=passwd123
```

#### Serviços

**Exibir lista e status dos serviços:**

```
ip service print
```

**Desativar múltiplos serviços:**

```
ip service set disabled=yes numbers=0,1,2,4,5,7
```
