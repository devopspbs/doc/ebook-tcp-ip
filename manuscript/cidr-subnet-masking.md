# CIDR e Sub-Redes

## Sub-Redes

**Tabela classes, máscaras e prefixos**

| Classe | Formato | Máscara (decimal) | Máscara (binário)            | Prefixo |
| ------ | ------- | ----------------- | ---------------------------- | ------- |
| A      | R.H.H.H | 255.0.0.0         | 11111111.0.0.0               | /8      |
| B      | R.R.H.H | 255.255.0.0       | 11111111.11111111.0.0        | /16     |
| C      | R.R.R.H | 255.255.255.0     | 11111111.11111111.11111111.0 | /24     |

**Tabela de "cola" para sub-redes:**

|     | 2^7 | 2^6 | 2^5 | 2^4 | 2^3 | 2^2 | 2^1 | 2^0 |
| --- | --- | --- | --- | --- | --- | --- | --- | --- |
|     | 128 | 64  | 32  | 16  | 8   | 4   | 2   | 1   |
| 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   | 0   |
| 128 | 1   | 0   | 0   | 0   | 0   | 0   | 0   | 0   |
| 192 | 1   | 1   | 0   | 0   | 0   | 0   | 0   | 0   |
| 224 | 1   | 1   | 1   | 0   | 0   | 0   | 0   | 0   |
| 240 | 1   | 1   | 1   | 1   | 0   | 0   | 0   | 0   |
| 248 | 1   | 1   | 1   | 1   | 1   | 0   | 0   | 0   |
| 252 | 1   | 1   | 1   | 1   | 1   | 1   | 0   | 0   |
| 254 | 1   | 1   | 1   | 1   | 1   | 1   | 1   | 0   |
| 255 | 1   | 1   | 1   | 1   | 1   | 1   | 1   | 1   |

**Cálculo de Sub-redes:**

```math
2^x >= s
```

Onde:
* "x" é o número de "0s" que deverão ser transformados em "1s";
* "s" é o número de sub-redes que se deseja obter.

**Cálculo de host:**

```math
2^y - 2 = h
```

Onde:
* "y" é o número de "0s" na máscara;
* "h" é o número de hosts possíveis por sub-rede;
* "-2" seria a exclusão dos endereços de rede e broadcast do total.

**Sub-redes válidas:**

```math
256 - m = i
```

Onde:
* "m" é o valor do último octeto com bits de sub-rede ativos;
* "i" representa o intervalo no qual as sub-redes ocorrem.

## CIDR

| Intervalo                     | Máscara     | Endereço de Rede |
|:----------------------------- |:----------- |:---------------- |
| 10.0.0.0    a 10.255.255.255  | 255.0.0.0   | 10.0.0.0/8       |
| 172.16.0.0  a 172.31.255.255  | 255.240.0.0 | 172.16.0.0/12    |
| 192.168.0.0 a 192.168.255.255 | 255.255.0.0 | 192.168.0.0/16   |
