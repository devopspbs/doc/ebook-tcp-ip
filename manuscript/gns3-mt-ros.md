# RouterOS no GNS3

## Criar Novo Projeto no GNS3

Um novo projeto pode ser criado na abertura do aplicativo. Ou a qualquer momento no menu **File > New blank project**, ou ainda nos atalhos ![](new-project.svg) na Toolbar e **Ctrl+N** no teclado.

![](images/gns3-new-project.png)

## Interface do GNS3

![](images/gns3-workspace-toolbars.png)

Na interface do usuário do GNS3 nós temos basicamente:

* **Workspace:** É onde arrastamos e soltamos as appliances para montar nossas topologias. (Destacado com a cor verde na figura anterior)

* **Toolbar:** A barra de ferramentas onde temos acesso rapidamente aos ícones que permitem executar as tarefas básicas do GNS3. (Destacado com a cor azul)

* **Devices Toolbar:** Onde encontramos as appliances instalas e disponíveis no Marketplace organizadas por categorias, e o botão de adicionar ligação entre elas. (Destacado com a cor vermelha)

* **Topology Summary:** Exibe os dispositivos no worksapce com seus respectivos status (on, off, suspenso) e conexões.

* **Servers Summary:** Exibe informações dos servers GNS3 e seus status.

* **Console:** Exibe mensagens de alertas, erros, problemas e etc.

## Adicionar a Appliance RouterOS

O dispositivo **MikroTik CHR** está disponível via **Devices Toolbar > Available appliances**. Usando o **Filter** é possível localizar facilmente o appliance.

![](images/add-mt-ros-chr.png)

Ao clicar, segurar e arrastar o appliance **MikroTik CHR** para o Workspace uma janela do GNS3 é aberta para nos auxiliar a obtê-la.

![](images/add-mt-ros-chr-1.png)

Após clicar no botão **Next >** vemos a janela onde selecionamos o tipo de servidor onde o appliance será executado seguido de **Next >** novamente.

![](images/add-mt-ros-chr-2.png)

Confirmando que está tudo okay prosseguimos com a instalação clicando em **Next >**.

![](images/add-mt-ros-chr-3.png)

Na janela seguinte selecionamos a imagem do CHR que será baixada, preferencialmente a mais atual, e clicamos no botão **Download**. Um link será aberto no navegador padrão do sistema operacional e download irá iniciar.

Perceba que o arquivo baixado está compactado, algo como **chr-6.41.4.img.zip**. Entretanto para importar a imagem no GNS3 precisamos descompactar/extrair primeiro. Vá até o diretório onde o arquivo foi baixado e descompacte/extraia a imagem.

Ainda na janela **Required files** do GNS3, clique no botão **Import** para indicar a localização da imagem do CHR, se o arquivo estiver no diretório padrão de downloads do sistema operacional nem precisa importar, basta apenas clicar no botão **Next >** com a imagem selecionada.

![](images/add-mt-ros-chr-4.png)

Uma vez que o arquivo tenha sido localizado basta clicar em **Next >** com a imagem selecionada.

![](images/add-mt-ros-chr-5.png)

Em seguida confirmamos a instalação da imagem clicando no botão **Yes** da janela de dialogo.

![](images/add-mt-ros-chr-6.png)

A próxima janela que vemos é da solução de virtualização onde podemos continuar clicando no botão **Next >**.

![](images/add-mt-ros-chr-7.png)

Após conferir o resumo clicamos no botão **Next >**.

![](images/add-mt-ros-chr-8.png)

Por fim temos a janela com algumas informações sobre o uso da appliance, bastando clicar em **Finish** para concluir.

![](images/add-mt-ros-chr-9.png)

Somos informados que o appliance foi instalado com sucesso.

![](images/add-mt-ros-chr-10.png)

Agora basta clicar, segurar, arrastar e soltar no Workspace para adicionar um dispositivo baseado nela no projeto.

![](images/add-mt-ros-chr-11.png)

## Cloud Hosted Router

Cloud Hosted Router (CHR) é uma versão do RouterOS feita para ser usada em ambientes virtuais. Embora a licença gratuita do CHR tenha certas limitações, como por exemplo, velocidade máxima de 1Mbps, ela permite acesso a todos os recursos do RouterOS.

![](images/mt-ros-chr-download.png)

Com relação as opções de download do CHR temos:

* **Arquivo IMG:** Imagem para soluções de virtualização como Xen, Qemu, KVM e baseadas.
* **Arquivo VMDK:** Imagem para soluções de virtualização da VMWare.
* **Arquivo VHDX:** Imagem para solução de virtualização da Microsoft Hyper-V.
* **Arquivo VDI:** Imagem para solução de virtualização Oracle VirtualBox.

## Dica: Configurações do GNS3

**View > Show the grid**: Exibir linhas da grade.

**View > Snap to grid**: Atrai objetos para os cantos das grade.

**View > Show/Hide interface labels**: Exibir ou ocultar os rótulos das interfaces dos appliances.

Na Devices Toolbar localize o nosso appliance MikroTik, em seguida clique **Botão Direito > Configure template**, e então configuramos da seguinte forma:

![](images/configure-template.png)

* Aba **General settings**
  - Template name = R
  - Default name format = {name}{0}

![](images/configure-template-1.png)

* Aba **Network**
  - Adapters = 5
  - Name format = ether{port1}

![](images/configure-template-2.png)

Para facilitar a localização do dispositivo com o novo nome selecione **Installed appliances** na Devices Toolbar.

![](images/configure-template-3.png)

## Interligar os Appliances

Agora que nós sabemos que para adicionar um appliance no GNS3 basta arrastar e soltar no Workspace, o mínimo que nós precisamos saber também é interligá-los.

Dica: Para adicionar múltiplos devices no GNS3 segure a tecla **Shift**, clique, arraste e solte a appliance no Workspace para abrir a janela e indicamos o número de dispositivos a serem criados.

![](images/gns3-add-nodes.png)

Selecione a ferramenta **Add a link**, ícone ![](images/add-link-1.svg) na Devices Toolbar, clique sobre a primeira appliance a ser conecta, em seguida escolha a porta desejada.

![](images/gns3-link-node-1.png)

Com a linha indicando o link ancorada ao primeiro appliance clique sobre o segundo repetindo a ação de selecionar uma das interfaces do dispositivo.

![](images/gns3-link-node-2.png)

Repare que a ferramenta **Add a link** continua ativa, indicado pelo ícone ![](images/add-link-1-cancel.svg). Para desativar podemos clicar novamente sobre o ícone da ferramenta na Devices Toolbar, ou clicar com o botão direito do mouse na área do Workspace, ou ainda simplesmente pressionar a tecla **Esc**.
