# Acesso ao RouterOS

## Meios de Acesso

* **Cabo Ehternet:** Usando o tradicional cabo de rede (par trançado de 8 vias com conector RJ-45) para interligar nosso Computador Desktop ou Laptop/Notebook ao dispositivo MikroTik RouterOS.

* **Wi-Fi:** Usando rede sem fio, essa opção é a preferida para os equipamentos da linha SOHO (Small Office Home Office).

* **Diretamente:** Como o RouterOS conta com uma versão para processadores x86 é possível ter o sistema rodando em um computador tipo PC e o acesso ser feito diretamente por meio de teclado e monitor.

## Primeiro Acesso

A seguir temos a mais comum configuração padrão dos dispositivos RouterOS, entretanto perceba que exitem variações dessa configuração conforme os tipos/linhas de produto.

* Endereço IP padrão: 192.168.88.1
* Usuário padrão: admin
* Senha padrão: (em branco)
* DHCP Client: porta 1 com firewall ativo

## WinBox

É um aplicativo de desktop para sistemas operacionais Microsoft Windows, mas que pode rodar em sistemas do tipo *nix por meio de softwares como o Wine, e usado para gerenciar dispositivos RouterOS.

![](https://wiki.mikrotik.com/images/a/a1/Wb-man-4.PNG)

## WebFig

Uma interface similar ao WinBox para gerenciar um dispositivo RouterOS mas com a vantagem de ser web, ou seja, pode ser acessada usando um navegador de internet.

![](https://wiki.mikrotik.com/images/7/73/Webfig-1.png)

## MikroTik Mobile App

Aplicativo para smartphones Android e iOS que permite gerenciar dispositivos RouterOS.

![](https://i.mt.lv/img/mt/v2/software/phones.jpg)

## QuickSet

Embora não seja mais uma opção ou forma para acessar o RouterOS nós não poderíamos de deixar de citar o QuickSet. Ele é um assistente de configuração do RouterOS que permite configurar com poucos cliques coisas, como por exemplo, redes wireless, acesso à internet, rede local, VPN, etc.

![](https://wiki.mikrotik.com/images/2/24/Quickset639.png)
